# Cryptic

> Tracks cryptic crossword puzzles, clues, and answers

## Overview

Cryptic is a tool I use when constructing cryptic crosswords for <https://cripsylemon.com>. It wraps a SQLite database of puzzles, words, and clues.

## Usage

Cryptic is a CLI for working with cryptic clues

Usage:
  cryptic [command]

Available Commands:
  add         Add a puzzle, word, and clue
  help        Help about any command
  load        Load a puzzle
  puzzles     List puzzles
  search      search
  words       List words

Flags:
  -h, --help   help for cryptic

Use "cryptic [command] --help" for more information about a command.

## License

Copyright &copy; 2020 Rob Warner

Licensed under the [MIT License](https://hoop33.mit-license.org/)

