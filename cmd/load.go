package cmd

import (
	"bufio"
	"fmt"
	"net/http"
	"os"
	"regexp"
	"strings"

	"github.com/jinzhu/gorm"

	"gitlab.com/hoop33/cryptic/model"

	"github.com/PuerkitoBio/goquery"
	"github.com/spf13/cobra"
)

func getDocumentFromURL(url string) (*goquery.Document, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	document, err := goquery.NewDocumentFromReader(response.Body)
	if err != nil {
		return nil, err
	}

	return document, nil
}

func getPuzzleNumber(document *goquery.Document) string {
	heading := document.Find("h1").First()
	if heading == nil {
		return ""
	}

	re := regexp.MustCompile("^Cryptic Crossword (\\d+)$")
	matches := re.FindStringSubmatch(heading.Text())
	if matches == nil {
		return ""
	}
	return matches[1]
}

func loadPuzzle(number string, document *goquery.Document) error {
	fmt.Println("Loading...")

	reader := bufio.NewReader(os.Stdin)

	re := regexp.MustCompile("(.*?).\\(.*\\)$")
	document.Find("td").Each(func(_ int, s *goquery.Selection) {
		matches := re.FindStringSubmatch(s.Text())
		if matches != nil {
			fmt.Printf("%s => ", matches[1])
			word, _ := reader.ReadString('\n')
			word = strings.Replace(word, "\n", "", -1)
			if err := addWord(number, word, matches[1]); err != nil {
				fmt.Println(err)
			}
		}
	})

	return nil
}

var loadCmd = &cobra.Command{
	Use:   "load",
	Short: "Load a puzzle",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			fmt.Println("URL")
			return
		}

		document, err := getDocumentFromURL(args[0])
		if err != nil {
			fmt.Println(err)
			return
		}

		puzzleNumber := getPuzzleNumber(document)
		if puzzleNumber == "" {
			fmt.Println("Can't find puzzle number")
			return
		}

		_, err = model.FindPuzzle(db, puzzleNumber)
		if err == nil {
			// No error means its already in database -- don't load it
			fmt.Printf("Puzzle %s is already in database\n", puzzleNumber)
			return
		}
		if !gorm.IsRecordNotFoundError(err) {
			// We got an error, and it wasn't that we couldn't find the puzzle
			fmt.Println(err)
			return
		}

		err = loadPuzzle(puzzleNumber, document)
	},
}

func init() {
	rootCmd.AddCommand(loadCmd)
}
