package cmd

import (
	"fmt"

	"gitlab.com/hoop33/cryptic/model"

	"github.com/spf13/cobra"
)

var puzzlesCmd = &cobra.Command{
	Use:   "puzzles",
	Short: "List puzzles",
	Run: func(cmd *cobra.Command, args []string) {
		var puzzles []model.Puzzle
		var err error

		if len(args) == 0 {
			puzzles, err = model.FindPuzzles(db)
			if err != nil {
				fmt.Println(err)
				return
			}
		} else {
			for _, arg := range args {
				puzzle, err := model.FindPuzzle(db, arg)
				if err != nil {
					fmt.Println(err)
					return
				}
				puzzles = append(puzzles, *puzzle)
			}
		}

		for _, puzzle := range puzzles {
			fmt.Println(puzzle.String())

		}
	},
}

func init() {
	rootCmd.AddCommand(puzzlesCmd)
}
