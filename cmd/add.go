package cmd

import (
	"fmt"

	"gitlab.com/hoop33/cryptic/model"

	"github.com/spf13/cobra"
)

func addWord(puzzleNum, wordText, clueText string) error {
	puzzle, err := model.FindOrCreatePuzzle(db, puzzleNum)
	if err != nil {
		return err
	}

	word, err := model.FindOrCreateWord(db, wordText)
	if err != nil {
		return err
	}

	clue := model.NewClue(puzzle.ID, word.ID, clueText)
	return clue.Save(db)
}

var addCmd = &cobra.Command{
	Use:   "add",
	Short: "Add a puzzle, word, and clue",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 3 {
			fmt.Println("<puzzle #> <word> <clue>")
			return
		}

		if err := addWord(args[0], args[1], args[2]); err != nil {
			fmt.Println(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(addCmd)
}
