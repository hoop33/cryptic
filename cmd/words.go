package cmd

import (
	"fmt"

	"gitlab.com/hoop33/cryptic/model"

	"github.com/spf13/cobra"
)

var wordsCmd = &cobra.Command{
	Use:   "words",
	Short: "List words",
	Run: func(cmd *cobra.Command, args []string) {
		var words []model.Word
		var err error

		if len(args) == 0 {
			words, err = model.FindWords(db)
			if err != nil {
				fmt.Println(err)
				return
			}
		} else {
			for _, arg := range args {
				word, err := model.FindWord(db, arg)
				if err != nil {
					fmt.Println(err)
					return
				}
				words = append(words, *word)
			}
		}

		for _, word := range words {
			fmt.Print(word.String())
		}
	},
}

func init() {
	rootCmd.AddCommand(wordsCmd)
}
