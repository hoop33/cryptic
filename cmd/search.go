package cmd

import (
	"fmt"
	"strings"

	"gitlab.com/hoop33/cryptic/model"

	"github.com/spf13/cobra"
)

var searchCmd = &cobra.Command{
	Use:   "search",
	Short: "search",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			fmt.Println("search <query>")
		} else {
			for _, arg := range args {
				fmt.Printf("Searching words for %s...\n", strings.ToUpper(arg))
				words, err := model.SearchWords(db, arg)
				if err != nil {
					fmt.Println(err)
					return
				}
				for _, word := range words {
					fmt.Println(word.String())
				}

				fmt.Printf("\nSearching clues for %s...\n", strings.ToUpper(arg))
				clues, err := model.SearchClues(db, arg)
				if err != nil {
					fmt.Println(err)
					return
				}
				for _, clue := range clues {
					fmt.Println(clue.String())
				}
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(searchCmd)
}
