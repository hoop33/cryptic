package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/hoop33/cryptic/model"
)

var reusedCmd = &cobra.Command{
	Use:   "reused",
	Short: "List any words used more than once",
	Run: func(cmd *cobra.Command, args []string) {
		words, err := model.ReusedWords(db)
		if err != nil {
			fmt.Println(err)
			return
		}

		for _, word := range words {
			fmt.Println(word.String())
		}
	},
}

func init() {
	rootCmd.AddCommand(reusedCmd)
}
