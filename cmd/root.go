package cmd

import (
	"fmt"
	"os"
	"path"

	"gitlab.com/hoop33/cryptic/model"

	"github.com/jinzhu/gorm"
	"github.com/spf13/cobra"
	"github.com/zchee/go-xdgbasedir"
)

var db *gorm.DB

var rootCmd = &cobra.Command{
	Use:   "cryptic",
	Short: "A CLI for working with cryptic clues",
}

func configPath() string {
	return path.Join(xdgbasedir.ConfigHome(), "cryptic")
}

func dbPath() string {
	return path.Join(configPath(), "cryptic.db")
}

// Execute runs the program
func Execute() {
	err := os.MkdirAll(configPath(), os.ModePerm)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	db, err = model.InitDB(dbPath(), false)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	defer db.Close()

	err = rootCmd.Execute()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}
