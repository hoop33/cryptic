package cmd

import (
	"fmt"

	"gitlab.com/hoop33/cryptic/model"

	"github.com/spf13/cobra"
)

var countsCmd = &cobra.Command{
	Use:   "counts",
	Short: "Show counts",
	Run: func(cmd *cobra.Command, args []string) {
		puzzles, err := model.CountPuzzles(db)
		if err != nil {
			fmt.Println(err)
			return
		}

		words, err := model.CountWords(db)
		if err != nil {
			fmt.Println(err)
			return
		}

		clues, err := model.CountClues(db)
		if err != nil {
			fmt.Println(err)
			return
		}

		fmt.Printf(`Puzzles: %d
Words:   %d
Clues:   %d
`, puzzles, words, clues)
	},
}

func init() {
	rootCmd.AddCommand(countsCmd)
}
