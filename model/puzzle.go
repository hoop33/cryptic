package model

import (
	"bytes"
	"fmt"
	"strconv"

	"github.com/jinzhu/gorm"
)

// Puzzle represents a single puzzle
type Puzzle struct {
	gorm.Model
	Number int
	Clues  []Clue
}

// FindPuzzles finds all the puzzles
func FindPuzzles(db *gorm.DB) ([]Puzzle, error) {
	var puzzles []Puzzle
	db.Order("number asc").Preload("Clues").Preload("Clues.Word").Find(&puzzles)
	return puzzles, db.Error
}

// FindPuzzle finds the puzzle with the specified number
func FindPuzzle(db *gorm.DB, number string) (*Puzzle, error) {
	n, err := strconv.Atoi(number)
	if err != nil {
		return nil, err
	}

	var puzzle Puzzle
	err = db.Where("number = ?", n).Preload("Clues").Preload("Clues.Word").First(&puzzle).Error
	return &puzzle, err
}

// FindOrCreatePuzzle finds or creates a puzzle with the specified number
func FindOrCreatePuzzle(db *gorm.DB, number string) (*Puzzle, error) {
	n, err := strconv.Atoi(number)
	if err != nil {
		return nil, err
	}

	var puzzle Puzzle
	if db.Where("number = ?", n).First(&puzzle).RecordNotFound() {
		puzzle.Number = n
		err = db.Create(&puzzle).Error
		return &puzzle, err
	}
	return &puzzle, nil
}

// CountPuzzles counts the number of puzzles
func CountPuzzles(db *gorm.DB) (int, error) {
	var count int
	db.Table("puzzles").Count(&count)
	return count, db.Error
}

// String returns a string representation of a puzzle
func (p *Puzzle) String() string {
	var buffer bytes.Buffer

	buffer.WriteString(fmt.Sprintf("Puzzle #%d\n", p.ID))
	for _, clue := range p.Clues {
		buffer.WriteString(clue.String())
		buffer.WriteString("\n")
	}

	return buffer.String()
}
