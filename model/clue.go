package model

import (
	"fmt"
	"strings"

	"github.com/jinzhu/gorm"
)

// Clue represents a clue used in a puzzle for a specific word
type Clue struct {
	gorm.Model
	Text     string
	PuzzleID uint
	Puzzle   Puzzle
	WordID   uint
	Word     Word
}

// NewClue creates a new clue
func NewClue(puzzleID, wordID uint, text string) *Clue {
	return &Clue{
		PuzzleID: puzzleID,
		WordID:   wordID,
		Text:     text,
	}
}

// FindClues finds all the clues
func FindClues(db *gorm.DB) ([]Clue, error) {
	var clues []Clue
	db.Group("puzzleID").Order("text asc").Find(&clues)
	return clues, db.Error
}

// CountClues counts the number of clues
func CountClues(db *gorm.DB) (int, error) {
	var count int
	db.Table("clues").Count(&count)
	return count, db.Error
}

// SearchClues searches partial strings
func SearchClues(db *gorm.DB, text string) ([]Clue, error) {
	var clues []Clue
	db.Order("text asc").Preload("Word").Where("text LIKE ?", fmt.Sprintf("%%%s%%", strings.ToUpper(text))).Find(&clues)
	return clues, db.Error
}

// Save saves a clue
func (c *Clue) Save(db *gorm.DB) error {
	return db.Create(c).Error
}

// String returns a string representation of a clue
func (c *Clue) String() string {
	return fmt.Sprintf("%s : %s", c.Word.Text, c.Text)
}
