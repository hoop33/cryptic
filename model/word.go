package model

import (
	"bytes"
	"fmt"
	"strings"

	"github.com/jinzhu/gorm"
)

// Word represents a word used in a puzzle
type Word struct {
	gorm.Model
	Text  string
	Clues []Clue
}

// FindWords finds all the words
func FindWords(db *gorm.DB) ([]Word, error) {
	var words []Word
	db.Order("text asc").Preload("Clues").Find(&words)
	return words, db.Error
}

// FindWord finds a word with the specified text
func FindWord(db *gorm.DB, text string) (*Word, error) {
	var word Word
	err := db.Where("upper(text) = ?", strings.ToUpper(text)).Preload("Clues").First(&word).Error
	return &word, err
}

// FindOrCreateWord finds or creates a word with the specified number
func FindOrCreateWord(db *gorm.DB, text string) (*Word, error) {
	var word Word
	t := strings.ToUpper(text)
	if db.Where("upper(text) = ?", t).First(&word).RecordNotFound() {
		word.Text = t
		err := db.Create(&word).Error
		return &word, err
	}
	return &word, nil
}

// SearchWords searches partial strings
func SearchWords(db *gorm.DB, text string) ([]Word, error) {
	var words []Word
	db.Order("text asc").Preload("Clues").Where("text LIKE ?", fmt.Sprintf("%%%s%%", strings.ToUpper(text))).Find(&words)
	return words, db.Error
}

// ReusedWords finds all words used more than once
func ReusedWords(db *gorm.DB) ([]Word, error) {
	// TODO figure out how to do this in SQL/gorm
	words, err := FindWords(db)
	if err != nil {
		return nil, err
	}

	reused := make([]Word, 0)
	for _, word := range words {
		if len(word.Clues) > 1 {
			reused = append(reused, word)
		}
	}

	return reused, nil
}

// CountWords counts the number of words
func CountWords(db *gorm.DB) (int, error) {
	var count int
	db.Table("words").Count(&count)
	return count, db.Error
}

// String returns this word as a string
func (w *Word) String() string {
	var buffer bytes.Buffer

	buffer.WriteString(fmt.Sprintf("%s\n", w.Text))

	for _, clue := range w.Clues {
		buffer.WriteString(fmt.Sprintf("%d : %s\n", clue.PuzzleID, clue.Text))
	}

	return buffer.String()
}
