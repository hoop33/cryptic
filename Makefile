PACKAGES = $(shell go list ./...)

default: build

install: build
	go install

build: check
	go build

check: vet lint test

vet:
	go vet $(PACKAGES)

lint:
	golint -set_exit_status $(PACKAGES)

test:
	go test -cover $(PACKAAGES)

deps:
	go get -u golang.org/x/lint/golint
